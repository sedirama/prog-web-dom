<?php

function smartcurl($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, "php-libcurl");
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

    $rawcontent = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);
    return [$rawcontent, $info];
}


function tmdbget($urlcomponent, $params=null) {
    $apikey = 'ebb02613ce5a2ae58fde00f4db95a9c1';
    $apiprefix = 'http://api.themoviedb.org/3/';  //3rd API version	
    $targeturl = $apiprefix . $urlcomponent . '?api_key=' . $apikey;
    $targeturl .= (isset($params) ? '&' . http_build_query($params) : '');
    list($content, $info) = smartcurl($targeturl);
    return $content;
}

//retorune -1 si l'acteur n'est pas dans le tableau sinon on retourne l'indice de cet acteur pour pouvoir le modifer aprés
function acteur_existant($tab,$name)
	{
	foreach($tab as $valeur =>$ligne)
		{
		if (strcmp($ligne['name'],$name)==0)
			{
			
			return $valeur;
			}
		}
	return -1;	
	}
	
?>	
