<?php
  require_once('function.php');

//--------------------------------------------------------------------------------------------------	
  //Préparation d'affichage de contenu HTML		
  ?>
  <html>
  <head>
     <title>Film</title>
  </head>
  <body>
  <table style = "border: 2px solid #333;">
  <thead style = "background-color: #333;color: #fff;" >
          <tr>
              <th colspan="1">Version Original</th>
              <th colspan="1">Version Française</th>
              <th colspan="1">Version Anglaise</th>
          </tr>
      </thead>
  <tbody>
  <tr>
  <td style="border: 2px solid #333;">   
  <?php
  
//------------------------------------------------------------------------------------------------


  //Affichage en deteil pour le film en version original
  $identifiant=$_GET['identifiant'];
  $url="movie/"."$identifiant";
  $Json = tmdbget($url,null);
  $donne=json_decode($Json);
  
  echo " le titre du film est : " .$donne->title;?> <br />
  <?php
  if ($donne->tagline!= "")
	  {
	  echo "la tagline est : ".$donne->tagline;?> <br />
	  <?php
	  }
  echo "Description : ".$donne->overview; ?> <br />





  <?php 
  //Affichage en deteil pour le film en version française
  //On recupère l'image à ce niveau pour la poster ensuite
  $path=$donne->poster_path;
  $image = "http://image.tmdb.org/t/p/w342/"."$path";
  ?>
  </td>
  <td style="border: 2px solid #333;">
  <?php
  $langage['language']="fr";
  
  $Json = tmdbget($url,$langage);
  $donne=json_decode($Json);
  echo " le titre du film est : " .$donne->title;?> <br />
  <?php
  if ($donne->tagline!= "")
	  {
	  echo "la tagline est : ".$donne->tagline;?> <br />
	  <?php
	  }
  echo "Description : ".$donne->overview; ?> <br />
  </td>
  <td style="border: 2px solid #333;">
  
  
  
  
  
  <?php
  //Affichage en deteil pour le film en version anglaise
  $langage['language']="en";
  $Json = tmdbget($url,$langage);
  $donne=json_decode($Json);
  echo " le titre du film est : " .$donne->title;?> <br />
  <?php
  if ($donne->tagline!= "")
  	  {
	  echo "la tagline est : ".$donne->tagline;?> <br />
	  <?php
	  }
  echo "Description : ".$donne->overview; ?><br />
  </td>
  </tr>
  </tbody>
  </table>
  <?php
  //Affichage de l'image précédement recuprer
  print '<img src="'.$image.'" alt="texte alternatif" />'; ?> <br />
  
  
  
  
  
  
  <?php
//-----------------------------------------------------------------------------------------------------
//Requete pour recuperer la collection de lord of the rings
  $tableau['query']="The Lord of the Rings";
  $url="search/collection";
  $Json = tmdbget($url,$tableau);
  $donne=json_decode($Json);
//A ce niveau on recupère l'identifiant du film pour faire une réquete et obtenir les films qui appartiennent à cette collection
  $id=$donne->results[0]->id;
  $url="/collection/"."$id";
  $Json = tmdbget($url,null);
  $donne=json_decode($Json);
  $film=$donne->parts;
//Ici on boucle sur chaque film et on procède à quelques traitment
  foreach($film as $key =>$ligne)
	  {
	  echo "le ".($key+1)."er/eme film est : \"".$ligne->title."\", identifié par le numero : ".$ligne->id.", sa date de sortie est le ".$ligne->release_date."\n";?> </br>
	  <?php
	  	
	//Recupération de la video et l'afficher en suite à travers une requete en utilisant le ID de la vidéo
	  $url="movie/".$ligne->id."/videos";
	  $Json=tmdbget($url, null);
	  $videos = json_decode($Json);
	  $url="https://www.youtube.com/embed/".$videos->results[0]->key;
	  echo '<iframe  width="420" height="345" src="'.$url.'"> </iframe> <br>';?> 
	  <?php
	//A ce niveau on recupere les données qui ont jouné sur le film
	  $identifiant=$ligne->id;
	  $url="movie/"."$identifiant"."/credits";
	  $Json = tmdbget($url,null);
	  $donne_acteur=json_decode($Json);
	  $acteurs=$donne_acteur->cast; 

	  
	//Ici on insère les informations de chaque emplyée dans un tableau associatif
	  foreach($acteurs as $key => $value)
		  {
		//On verifie d'abord que c'est bien un acteur
		  if (strcmp($value->known_for_department,"Acting")==0)
		  	  {
		  	//Un petit test pour savoir si la variable "acteur_completter est deja crée ou pas 
			  if(isset($acteurs_completer)==1)
		  		  { 
		  		//A ce niveau on evite la duplication 
		  		  $condition = acteur_existant($acteurs_completer,$value->name);
				  if($condition==-1)
					{
			 		$acteurs_completer[]=array('name'=>$value->name,'role'=>$value->character,'credit_id'=>$value->credit_id,'nombre_apparition'=>1);
			 		}
			 	  else
			 	  	{
			 	      // si l'acteur existe deja on incrémente son nombre d'apparition
			 	  	$acteurs_completer[$condition]['nombre_apparition']++;
			 	  	}
			 	  	
			 	  }
			   else
			 	  {
			 	  $acteurs_completer[]=array('name'=>$value->name,'role'=>$value->character ,'credit_id'=>$value->credit_id,'nombre_apparition'=>1);
			 	  }
			  }
		  }
	  }
	

	
	

 // Affige de tout les acteurs avec  le nombre d'apparition et leurs role
 echo "les acteurs qui ont joué dans les films sont: " ?> <br> <br>
 <?php	
  foreach ($acteurs_completer as $key =>$ligne)
	{
	$credit_idt=$ligne['credit_id'];
	$url='https://api.themoviedb.org/3/credit/'."$credit_idt"."?api_key=ebb02613ce5a2ae58fde00f4db95a9c1";
      //Affichage du nom avec un lien rebont 
	echo '<a href="'.$url.'">'.$ligne['name'].'</a>';
	$nombre=$ligne['nombre_apparition'];
	echo " a participé sous le role de \"".$ligne['role']."\" et est apapru ".$nombre." fois";?> </br>
	<?php
	}
	?>
	</br>
	</br>

<?php
 //Affichage des acteurs qui ont joué le rol de hobbit

  echo "les acteurs qui ont joué le role de Hobbit sont:" ?> <br><br>
  <?php	
  foreach ($acteurs_completer as $key =>$ligne)
	{
	//on test si le role contient la chaine de caractère Hobbt
	if (strpos($ligne['role'], "Hobbit")!== FALSE)
		{
		echo" l'acteur ".$ligne['name']." a participé sous le role \"".$ligne['role']."\"\n";?> </br>
		<?php
		}
	}	
  ?>
  </body>
  </html>
