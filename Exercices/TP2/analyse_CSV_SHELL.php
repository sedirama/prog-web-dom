<?php
function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

function initAccesspoint($row) {
  return array(
      'name' => $row[0], //string
      'adr' => $row[1],  //string
      'lon' => $row[2],  //float, in decimal degrees
      'lat' => $row[3]   //float, in decimal degrees
     );  
}


function distance ($p, $q) {
  $scale = 10000000 / 90; 
  $a = ((float)$p['lon'] - (float)$q['lon']);
  $b = (cos((float)$p['lat']/180.0*M_PI) * ((float)$p['lat'] - (float)$q['lat']));
  $res = $scale * sqrt( $a**2 + $b**2 );
  return (float)sprintf("%5.1f", $res);
}

function geopoint($lon, $lat) {
  return ['lon'=>$lon, 'lat'=>$lat];
}




$lines = file('borneswifi_EPSG4326.csv');


foreach ($lines as $line_num => $line) {

    $temp=str_getcsv($line, ",");
    $tableau[$line_num]=initAccesspoint($temp);
    
}


echo "les antennes acompagner de la distance \n";

foreach ($tableau as $line_num => $line) {

    echo $line['name']."  sa distance est de  ".distance(geopoint( 5.72752, 45.19102),$line)."\n";
  
    
}

echo "--------------------------------------------------------------------\n \n";
echo "les antennes les plus proches\n";

foreach ($tableau as $line_num => $line) {

     if (distance(geopoint( 5.72752, 45.19102),$line)<200)
    	{
    	$point_proche[]=['name'=>$line['name'],'dist'=>distance(geopoint( 5.72752, 45.19102),$line)];
    	}
    
}



$point_proche = array_orderby($point_proche, 'dist', SORT_ASC);


$N=$argv[1];

$i=0;
while($i < $N)
	{
	echo "classé en " .($i+1). " en postion on a ".$point_proche[$i]['name'];
	print("\n");
	$i=$i+1;	
	}

echo"--------------------------------------------------------------------\n \n";
echo" les antennes acompagner de leur adresse \n\n";

foreach ($tableau as $line_num => $line) {
    	$url = 'https://api-adresse.data.gouv.fr/reverse/?lon='.$line['lon'].'&lat='.$line['lat'].'';
    	$json = @file_get_contents($url);
   	 $data = json_decode($json);
   	 $tableau[$line_num]['label'] =  $data->features[0]->properties->label;
   	 echo $tableau[$line_num]['name']." est à l'adresse ".$tableau[$line_num]['label']."\n";

}

    

    


?>
