<?php

function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

function initAccesspoint2($row) {
  return array(
      'id' => $row[0], 
      'opr' => $row[3], 
      'adresse' => $row[7],  
     );  
}

function distance ($p, $q) {
  $scale = 10000000 / 90; 
  $a = ((float)$p['lon'] - (float)$q['lon']);
  $b = (cos((float)$p['lat']/180.0*M_PI) * ((float)$p['lat'] - (float)$q['lat']));
  $res = $scale * sqrt( $a**2 + $b**2 );
  return (float)sprintf("%5.1f", $res);
}


function geopoint($lon, $lat) {
  return ['lon'=>$lon, 'lat'=>$lat];
}


$lines = file('DSPE_ANT_GSM_EPSG4326.csv');


foreach ($lines as $line_num => $line) {
    $temp=str_getcsv($line, ",");
    $tableau[$line_num]=initAccesspoint2($temp);   
}


$x= $_GET['lon'];
$y = $_GET['lat'];
$N = $_GET['top'];

foreach ($tableau as $line_num => $line) {
	
	$adresse=urlencode($line['adresse']);
    	$json = @file_get_contents('https://api-adresse.data.gouv.fr/search/?q='.$adresse.'');
   	$data = json_decode($json);
   	$tableau[$line_num]['lon'] = $data->features[0]->geometry->coordinates[0];
   	$tableau[$line_num]['lat'] = $data->features[0]->geometry->coordinates[1];
 
}

foreach ($tableau as $line_num => $line) {

     if (distance(geopoint( $x, $y),$line)<200)
    	{
    	$point_proche[]=['id'=>$line['id'],'opr'=>$line['opr'] ,'dist'=>distance(geopoint( $x, $y),$line)];
    	}
    
}
$point_proche = array_orderby($point_proche, 'dist', SORT_ASC);
echo "--------------------------------------------------------------------";?> <br /> <br />
<?php
echo "les antennes les plus proches\n";?> <br />
<?php
$i=0;
while($i < $N)
	{
	if (strcmp($point_proche[$i]['opr'],$_GET['operateur'])==0)
		{
		echo $point_proche[$i]['id'];?> <br />
		<?php
		}
	$i=$i+1;	
	}




?>
