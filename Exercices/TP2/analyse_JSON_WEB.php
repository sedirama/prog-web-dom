<?php
function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

function initAccesspoint($row) {
  return array(
      'name' => $row[0], //string
      'adr' => $row[1],  //string
      'lon' => $row[2],  //float, in decimal degrees
      'lat' => $row[3]   //float, in decimal degrees
     );  
}


function distance ($p, $q) {
  $scale = 10000000 / 90; 
  $a = ((float)$p['lon'] - (float)$q['lon']);
  $b = (cos((float)$p['lat']/180.0*M_PI) * ((float)$p['lat'] - (float)$q['lat']));
  $res = $scale * sqrt( $a**2 + $b**2 );
  return (float)sprintf("%5.1f", $res);
}

function geopoint($lon, $lat) {
  return ['lon'=>$lon, 'lat'=>$lat];
}

$file = 'borneswifi_EPSG4326.json'; 
$data = file_get_contents($file);
$lines = json_decode($data,true); 


for ($i=0; $i < 68 ; $i++)
	{
	$resultat[$i]=$lines['features'][$i]['properties'];
	}
	

foreach($resultat as $tableau_num => $besoin)
	{
	$k=0;
	foreach($besoin as $i => $line)
		{
		$conversion[$k]=$line;
		$k++;
		}
	$tableau[$tableau_num] = initAccesspoint($conversion);
	}
	

$x= $_GET['lon'];
$y = $_GET['lat'];
$N = $_GET['top'];


foreach ($tableau as $line_num => $line) {

     if (distance(geopoint( $x, $y),$line)<200)
    	{
    	$point_proche[]=['name'=>$line['name'],'dist'=>distance(geopoint( $x, $y),$line)];
    	}
    
}

$point_proche = array_orderby($point_proche, 'dist', SORT_ASC);
$i=0;

$point_proche = array_orderby($point_proche, 'dist', SORT_ASC);
echo "--------------------------------------------------------------------";?> <br /> <br />
<?php
echo "les antennes les plus proches\n";?> <br />
<?php

while($i < $N)
	{
	echo $point_proche[$i]['name'];?> <br />
	<?php
	print("\n");
	$i=$i+1;	
	}

echo "--------------------------------------------------------------------";?> <br /> <br />
<?php
echo "les antennes avec leurs adresses\n";?> <br />
<?php

foreach ($tableau as $line_num => $line) {

    	$url = 'https://api-adresse.data.gouv.fr/reverse/?lon='.$line['lon'].'&lat='.$line['lat'].'';
    	$json = @file_get_contents($url);
   	 $data = json_decode($json);
   	 $tableau[$line_num]['label'] =  $data->features[0]->properties->label;
   	 echo $tableau[$line_num]['name']." est à l'adresse ".$tableau[$line_num]['label']; ?> <br />
<?php
}



?>
