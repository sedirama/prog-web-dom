<html>
<head>
  <title>Multiplication Table</title>
</head>
<body>

  <h2>Multiplication Table</h2>
  <form method="get" action="multiplication.php">
  <label for="rows">How many rows for your multiplication table?</label><br><br>
  <input type="text" id="rows" name="rows"><br><br>
  <label for="cols">How many columns for your multiplication table?</label><br><br>
  <input type="text" id="cols" name="cols"><br><br>
  <label for="select">Select a specific row in the table:</label><br><br>
  <input type="text" id="select" name="select"><br><br>
  <input type="submit" name="SubmitButton" value="Submit"><br><br><br>
  </form>

</body>

  <?php

    if(isset($_GET['SubmitButton'])) {

      if(isset($_GET['rows'], $_GET['cols']) && $_GET['rows'] != "" && $_GET['cols'] != "") {
        $rows = $_GET['rows'];
        $cols = $_GET['cols'];
      } else {
        $rows = 10;
        $cols = 10;
      }

      if(isset($_GET['select'])) {
        $select = $_GET['select'];
      } else {
        $select = null;
      }

      createTable($rows, $cols, $select);

    }

    function createTable($rows, $cols, $select) {
      $j = 1;
      echo "<table border='1' width='600' cellspacing='0' cellpadding='5'>";
      for($i = 1; $i <= $rows; $i++) {
        if($select == $i){
          echo "<tr style='background-color:#ffff00'>";
        } else {
          echo "<tr>";
        }
        while($j <= $cols) {
          echo "<td>" . $i*$j . "</td>";
          $j = $j + 1;
        }
        echo "</tr>";
   		  $j = 1;
      }
      echo "</table>";
    }

  ?>


</html>
